/**
 * @param str string
 * @return string
 */
export declare function hex_md5(s: string): string;
/**
 * @param str string
 * @return string
 */
export declare function b64_md5(s: string): string;
