## md5

前端加密模块

## 安装

```bash
npm i @rocksandy/md5
```

## 使用

```js
import { hexMd5 } from '@util/md5';

const result = hexMd5('password');
```
